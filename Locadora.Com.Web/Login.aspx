﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Locadora.Com.Web.Login" %>

<!DOCTYPE html>
<html>
<head>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../CSS/bootstrap-grid.min.css" rel="stylesheet" />
    <link href="../CSS/bootstrap-reboot.min.css" rel="stylesheet" />
    <link href="../CSS/dataTables.bootstrap4.css" rel="stylesheet" />
    <link href="../CSS/sb-admin.min.css" rel="stylesheet" />
    <link href="../Content/font-awesome.min.css" rel="stylesheet" />
    <title></title>
  </head>
<body>
    <div class="container" style="margin-top: 150px">
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <div class="card">
                    <h5 class="card-header" style="text-align: center">SUPER LOCADORA - Versão 1.0</h5>
                    <div class="card-body" style="text-align: center">
                        <form id="frmLogin" role="form" runat="server">
                            <fieldset>
                                <div style="text-align: center; margin-bottom: 10px; padding-top: 10px;">
                                    <img class="profile-img center-block"
                                        src="../img/generic-logo.jpg" alt="" style="width: 130px; height: 60px;" />
                                </div>
                                <div class="row">
                                    <div class="col-sm-12" style="text-align: center; padding-left: 10%; padding-right: 10%;">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="glyphicon glyphicon-user"></i>
                                                </span>
                                                <input class="form-control" runat="server" placeholder="Seu nome de usuário" name="loginname" id="txtLogin" type="text" maxlength="30" required="required" autofocus="autofocus" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="glyphicon glyphicon-lock"></i>
                                                </span>
                                                <input class="form-control" runat="server" placeholder="Sua senha de acesso" name="password" id="txtSenha" type="password" value="" maxlength="10" required="required" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lblMsgSis" runat="server" Text="Mensagem do sistema" CssClass="label-danger" ForeColor="Red" Visible="False" BackColor="White"></asp:Label>
                                            <br />
                                            <asp:Button ID="btnEntrar" CssClass="btn btn-lg btn-primary btn-block" BackColor="#d78100" runat="server" Text="Entrar" UseSubmitBehavior="true" OnClick="btnEntrar_Click" />
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                        <div class="card-footer bg-transparent border-success">                        
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3"></div>
        </div>
    </div>

     <div class="backstretch" style="left: 0px; top: 0px; overflow: hidden; margin: 0px; padding: 0px;
                 height: 1014px; width: 1920px; z-index: -999999; position: fixed;">
        <img src="img/UG144.jpg" class="img-responsive" style="position: absolute; margin: 0px; padding: 0px;
                 height: 1229px; width: 1920px; border:none; max-height: none; max-width: none; left: 0px; top: 0px;" />
    </div>

    <script src="Scripts/jquery-3.3.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>

    <script type="text/javascript">
        <!-- VALIDA SE CAPSLOCK DO TECLADO ESTÁ ATIVO-->
        $(document).ready(function () {
            $('input').keypress(function (e) {
                var s = String.fromCharCode(e.which);
                if ((s.toUpperCase() === s && s.toLowerCase() !== s && !e.shiftKey) ||
                   (s.toUpperCase() !== s && s.toLowerCase() === s && e.shiftKey)) {
                    if ($('#capsalert').length < 1) $(this).after('<br /><div class="col-md-4"><p><b style="color:blue; text-align:center;" id="capsalert">CapsLock on.</b></p></div>');
                } else {
                    if ($('#capsalert').length > 0) $('#capsalert').remove();
                }
            });
        });
    </script>
</body>
</html>