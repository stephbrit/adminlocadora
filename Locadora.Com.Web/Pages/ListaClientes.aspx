﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Locadora.Master" AutoEventWireup="true" CodeBehind="ListaClientes.aspx.cs" Inherits="Locadora.Com.Web.Pages.ListaClientes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">    
    <div class="row">
        <div class="col-lg-12">
                <h2 class="page-header"><i class="fa fa-address-book"></i> Clientes <small>- lista</small></h2>
         </div>       
    <div class="table">       
        <div class="container-fluid">           
            <asp:ScriptManager runat="server" />
            <asp:UpdatePanel runat="server" ID="upPanel" UpdateMode="Conditional" ViewStateMode="Enabled" ClientIDMode="Static">
                    <ContentTemplate> 
                    <table id="tabCli" class="table table-responsive table-bordered table-striped"
                                 role="grid" aria-describedby="datatable_info">
                        <asp:GridView ID="gridClientes" runat="server" CssClass="table table-bordered table-striped" 
                            GridLines="None" AutoGenerateColumns="False" OnRowCommand="gridClientes_RowCommand">
                                <Columns>
                                    <asp:BoundField DataField="IdCliente" HeaderText="ID_CLI" Visible="false" />
                                    <asp:BoundField DataField="DsNomeCli" HeaderText="NOME" />
                                    <asp:BoundField DataField="NmCpf" HeaderText="CPF" />
                                    <asp:BoundField DataField="DsEndereco" HeaderText="ENDEREÇO" />
                                    <asp:BoundField DataField="NmCep" HeaderText="CEP" />
                                </Columns>
                           <RowStyle CssClass="cursor-pointer" />
                        </asp:GridView>   
                    </table>
             </ContentTemplate>
           </asp:UpdatePanel>
        </div>
    </div>
</div>
    <script type="text/javascript">
    //datatables
    $(document).ready(function () {
        var handleDataTableButtons = function () {
            if ($("#tabCli").length) {
                $("#tabCli").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                      {
                          extend: "copy",
                          className: "btn-sm btn-info",
                          text: "Copiar"
                      },
                      {
                          extend: "excel",
                          className: "btn-sm btn-success",
                          text: "Exportar excel"
                      },
                      {
                          extend: "pdfHtml5",
                          className: "btn-sm btn-warning",
                          text: "Gerar PDF"
                      },
                      {
                          extend: "print",
                          className: "btn-sm btn-primary",
                          text: " Imprimir"
                      },
                    ],
                    responsive: true
                });
            }
        };

        TableManageButtons = function () {
            "use strict";
            return {
                init: function () {
                    handleDataTableButtons();
                }
            };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
            keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
            ajax: "js/datatables/json/scroller-demo.json",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
        });

        $('#datatable-fixed-header').DataTable({
            fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
            'order': [[1, 'asc']],
            'columnDefs': [
              { orderable: false, targets: [0] }
            ]
        });
        $datatable.on('draw.dt', function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-green'
            });
        });

        TableManageButtons.init();
    });
    </script>    
</asp:Content>