﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Locadora.Master" AutoEventWireup="true" CodeBehind="CadLocacoes.aspx.cs" Inherits="Locadora.Com.Web.Pages.CadLocacoes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-lg-12">
                <h2 class="page-header"><i class="fa fa-cart-arrow-down"></i> Locações <small>- cadastro</small></h2>
         </div>
        <div class="form-control"">
            <div class="form-group">
                <div class="col-md-4">
                     <label>Nome Cliente</label>                                                         
                    <asp:DropDownList runat="server" ID="listaCli" class="form-control" CausesValidation="True">                       
                    </asp:DropDownList>
                </div>
                <div class="col-md-4">
                     <label>Nome do Filme</label>
                    <asp:DropDownList runat="server" ID="listaFilme" class="form-control" CausesValidation="True">                       
                    </asp:DropDownList>                                                   
                </div>               
                <div class="col-md-4">
                     <label>Data Programada Devolução</label>                                                         
                     <input class="form-control" runat="server" placeholder="Dia/Mês/Ano" name="txtDtDev" 
                         id="txtDtDev" type="date"/>
                </div>                
            <br />
                <div class="form-group">
                    <div class="col-md-4">
                     <button id="btnGravar" name="btnGravar" type="submit" runat="server" class="btn btn-success pull-right" 
                         onserverclick="btnGravar_Click"><i class="fa fa-save"></i> Gravar Registro</button>
                    </div>
               </div>
                <asp:Label Text="" runat="server" id="lblMsgSis" Visible="false"/>
        </div>
      </div>
    </div>
</asp:Content>