﻿using BLL.Locadora;
using MOD.Locadora;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Locadora.Com.Web.Pages
{
    public partial class CadFilmes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGravar_Click(object sender, EventArgs e)
        {
            var modFilm = new modTbFilmes();
            var objBllFilm = new bllFilmes();

            try
            {
                modFilm.DsNomeFilme = txtNome.Value;
                modFilm.NmAnoLancamento = Convert.ToInt32(txtAno.Value);

                objBllFilm.pubCadastraFilme(modFilm);

                lblMsgSis.Text = "Filme cadastrado com sucesso!";
                lblMsgSis.ForeColor = Color.Green;
                lblMsgSis.Visible = true;

                prisLimpaCampos();
            }
            catch (Exception ex)
            {
                lblMsgSis.Text = ex.Message.ToString();
                lblMsgSis.ForeColor = Color.Red;
                lblMsgSis.Visible = true;

                prisLimpaCampos();
            }
        }

        protected void prisLimpaCampos()
        {
            txtNome.Value = string.Empty;
            txtAno.Value = string.Empty;          
        }
    }
}