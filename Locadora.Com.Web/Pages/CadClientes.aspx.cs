﻿using BLL.Locadora;
using MOD.Locadora;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Locadora.Com.Web.Pages
{
    public partial class CadClientes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGravar_Click(object sender, EventArgs e)
        {
            var modCli = new modTbClientes();
            var objBllCli = new bllClientes();

            try
            {
                modCli.DsNomeCli = txtNome.Value;
                modCli.NmCpf = Convert.ToDouble(txtCpf.Value);
                modCli.DsEndereco = txtEndereco.Value;
                modCli.NmCep = txtCep.Value;

                objBllCli.pubCadastraCliente(modCli);

                lblMsgSis.Text = "Cliente cadastrado com sucesso!";
                lblMsgSis.ForeColor = Color.Green;
                lblMsgSis.Visible = true;

                prisLimpaCampos();
            }
            catch (Exception ex)
            {
                lblMsgSis.Text = ex.Message.ToString();
                lblMsgSis.ForeColor = Color.Red;
                lblMsgSis.Visible = true;

                prisLimpaCampos();
            }
        }

        protected void prisLimpaCampos()
        {
            txtNome.Value = string.Empty;         
            txtCpf.Value = string.Empty;
            txtEndereco.Value = string.Empty;
            txtCep.Value = string.Empty;
        }
    }
}