﻿using BLL.Locadora;
using MOD.Locadora;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Locadora.Com.Web.Pages
{
    public partial class CadLocacoes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var idUsuario = Session["idUsuario"];

            if (idUsuario != null)
            {
                prisCarregaListaCli();
                prisCarregaListaFilmes();
            }
            else
            {
                Response.Redirect("~/Login.aspx");
            }            
        }

        protected void btnGravar_Click(object sender, EventArgs e)
        {
            var modLoc = new modTbLocacao();
            var objBllLoc = new bllLocacoes();

            try
            {
                modLoc.IdCliente = Convert.ToInt32(listaCli.SelectedValue);
                modLoc.IdFilme = listaFilme.SelectedIndex;
                modLoc.IdUsuarioLocacao = Convert.ToInt32(Session["idUsuario"]);
                modLoc.DtProgramadaDevolucao = Convert.ToDateTime(txtDtDev.Value);

                objBllLoc.pubCadastraLocacao(modLoc);

                lblMsgSis.Text = "Locação cadastrado com sucesso!";
                lblMsgSis.ForeColor = Color.Green;
                lblMsgSis.Visible = true;

                prisLimpaCampos();
            }
            catch (Exception ex)
            {
                lblMsgSis.Text = ex.Message.ToString();
                lblMsgSis.ForeColor = Color.Red;
                lblMsgSis.Visible = true;

                prisLimpaCampos();
            }
        }

        protected void prisLimpaCampos()
        {
            listaCli.SelectedValue = string.Empty;
            listaFilme.SelectedValue = string.Empty;
            txtDtDev.Value = string.Empty;
        }

        protected void prisCarregaListaCli()
        {            
            var listClientes = new List<modTbClientes>();
            var objBllLoc = new bllClientes();

            try
            {
                listClientes = objBllLoc.pubListaTodosOsClientes().ToList();

                if(listClientes != null)
                {
                    listaCli.DataSource = listClientes;
                    
                    listaCli.DataValueField = "IdCliente";
                     
                    listaCli.DataTextField = "DsNomeCli";
                   
                    listaCli.DataBind();

                    listaCli.Items.Add(new ListItem("Selecione...", "0"));                        
                   
                    listaCli.SelectedValue = Convert.ToString(0);
                }

            }
            catch (Exception ex)
            {
                lblMsgSis.Text = ex.Message.ToString();
                lblMsgSis.ForeColor = Color.Red;
                lblMsgSis.Visible = true;

                prisLimpaCampos();
            }
        }

        protected void prisCarregaListaFilmes()
        {
            var listasFilmes = new List<modTbFilmes>();
            var objBllFilmes = new bllFilmes();

            try
            {
                listasFilmes = objBllFilmes.pubListaTodosOsFilmes();

                if (listasFilmes != null)
                {
                    foreach (var f in listasFilmes)
                    {
                        listaFilme.DataValueField = Convert.ToString(f.IdFilme);
                        listaFilme.DataTextField = f.DsNomeFilme;
                        listaFilme.Items.Add(new ListItem("Selecione...", "0"));
                    }

                    listaFilme.SelectedIndex = 0;                   
                }

            }
            catch (Exception ex)
            {
                lblMsgSis.Text = ex.Message.ToString();
                lblMsgSis.ForeColor = Color.Red;
                lblMsgSis.Visible = true;

                prisLimpaCampos();
            }
        }
    }
}