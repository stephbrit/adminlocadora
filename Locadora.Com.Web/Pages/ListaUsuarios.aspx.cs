﻿using BLL.Locadora;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Locadora.Com.Web.Pages
{
    public partial class ListaUsuarios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            prisCarregaGrid();
        }

        protected void gridUsuario_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void prisCarregaGrid()
        {
            try
            {
                var objBllUsr = new bllUsuarios();

                gridUsuario.DataSource = objBllUsr.pubListaTodosOsUsuarios();
                gridUsuario.DataBind();
                upPanel.Visible = true;
            }
            catch (Exception ex)
            {
                throw new Exception("Problema ao carregar eventos: - " + ex.Message.ToString());
            }
        }
    }
}