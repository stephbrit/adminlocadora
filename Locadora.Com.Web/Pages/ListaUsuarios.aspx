﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Locadora.Master" AutoEventWireup="true" CodeBehind="ListaUsuarios.aspx.cs" Inherits="Locadora.Com.Web.Pages.ListaUsuarios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-lg-12">
                <h2 class="page-header"><i class="fa fa-user"></i> Usuário <small>- lista</small></h2>
         </div>       
    <div class="table">       
        <div class="container-fluid">           
            <asp:ScriptManager runat="server" />
            <asp:UpdatePanel runat="server" ID="upPanel" UpdateMode="Conditional" ViewStateMode="Enabled" ClientIDMode="Static">
                    <ContentTemplate> 
                    <table id="tabUser" class="table table-responsive table-bordered table-striped"
                                 role="grid" aria-describedby="datatable_info">
                        <asp:GridView ID="gridUsuario" runat="server" CssClass="table table-bordered table-striped" 
                            GridLines="None" AutoGenerateColumns="False" OnRowCommand="gridUsuario_RowCommand">
                                <Columns>
                                    <asp:BoundField DataField="IdUsuario" HeaderText="ID_USUÁRIO" Visible="false" />
                                    <asp:BoundField DataField="UsrNome" HeaderText="NOME" />
                                    <asp:BoundField DataField="DsLogin" HeaderText="LOGIN" />
                                    <asp:BoundField DataField="NmCpf" HeaderText="CPF" />
                                    <asp:BoundField DataField="NmIdade" HeaderText="IDADE" />
                                    <asp:BoundField DataField="NmCargo" HeaderText="CARGO" />
                                </Columns>
                           <RowStyle CssClass="cursor-pointer" />
                        </asp:GridView>   
                    </table>
             </ContentTemplate>
           </asp:UpdatePanel>
        </div>
    </div>
</div>
</asp:Content>