﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Locadora.Master" AutoEventWireup="true" CodeBehind="CadClientes.aspx.cs" Inherits="Locadora.Com.Web.Pages.CadClientes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div class="row">
        <div class="col-lg-12">
                <h2 class="page-header"><i class="fa fa-address-book"></i> Clientes <small>- cadastro</small></h2>
         </div>
        <div class="form-control"">
            <div class="form-group">
            <div class="col-md-4">
                 <label>Nome</label>                                                         
                 <input class="form-control" runat="server" placeholder="Nome Completo" name="txtNome" id="txtNome" type="text" maxlength="100" />                              
            </div>
            <div class="col-md-4">
                 <label>CPF</label>                                                         
                 <input class="form-control" runat="server" placeholder="CPF Somente números" name="txtCpf" id="txtCpf" type="text" maxlength="11" />                              
            </div>
            <div class="col-md-4">
                 <label>Endereço</label>                                                         
                 <input class="form-control" runat="server" placeholder="Endereço" name="txtEndereco" id="txtEndereco" type="text" maxlength="100" />                              
            </div>
            <div class="col-md-4">
                 <label>Cep</label>                                                         
                 <input class="form-control" runat="server" placeholder="00000-000" name="txtCep" id="txtCep" type="text" maxlength="9" />                              
            </div>            
            <br />
                <div class="form-group">
                    <div class="col-md-4">
                     <button id="btnGravar" name="btnGravar" type="submit" runat="server" class="btn btn-success pull-right" 
                         onserverclick="btnGravar_Click"><i class="fa fa-save"></i> Gravar Registro</button>
                    </div>
               </div>
                <asp:Label Text="" runat="server" id="lblMsgSis" Visible="false"/>
        </div>
      </div>
    </div>
        
</asp:Content>
