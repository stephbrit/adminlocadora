﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Locadora.Master" AutoEventWireup="true" CodeBehind="CadFilmes.aspx.cs" Inherits="Locadora.Com.Web.Pages.CadFilmes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-lg-12">
                <h2 class="page-header"><i class="fa fa-file-video-o"></i> Filme <small>- cadastro</small></h2>
         </div>
        <div class="form-control"">
            <div class="form-group">
                <div class="col-md-4">
                     <label>Nome do Filme</label>                                                         
                     <input class="form-control" runat="server" placeholder="Nome do Filme" name="txtNome" id="txtNome" type="text" maxlength="50" />                              
                </div>
                <div class="col-md-4">
                     <label>Ano Lançamento</label>                                                         
                     <input class="form-control" runat="server" placeholder="Ano de Lançamento - YYYY" name="txtAno" id="txtAno" type="text" maxlength="4" />                              
                </div>                
            <br />
                <div class="form-group">
                    <div class="col-md-4">
                     <button id="btnGravar" name="btnGravar" type="submit" runat="server" class="btn btn-success pull-right" 
                         onserverclick="btnGravar_Click"><i class="fa fa-save"></i> Gravar Registro</button>
                    </div>
               </div>
                <asp:Label Text="" runat="server" id="lblMsgSis" Visible="false"/>
        </div>
      </div>
    </div>
</asp:Content>
