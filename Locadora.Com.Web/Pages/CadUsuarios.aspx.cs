﻿
using BLL.Locadora;
using MOD.Locadora;
using System;
using System.Drawing;

namespace Locadora.Com.Web.Pages
{
    public partial class CadUsuarios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          
        }

        protected void btnGravar_Click(object sender, EventArgs e)
        {
            var modU = new modTbUsuarios();
            var objBllU = new bllUsuarios();

            try
            {
                modU.nomeUsuario = txtNome.Value;
                modU.idade = Convert.ToInt16(txtIdade.Value);
                modU.cpfUsuario = Convert.ToDouble(txtCpf.Value);
                modU.cargo = txtCargo.Value;
                modU.dsLogin = txtLogin.Value;
                modU.dsSenha = txtSenha.Value;

                objBllU.pubCadastraNovoUsuario(modU);

                lblMsgSis.Text = "Usuário cadastrado com sucesso!";
                lblMsgSis.ForeColor = Color.Green;
                lblMsgSis.Visible = true;

                prisLimpaCampos();
            }
            catch (Exception ex)
            {
                lblMsgSis.Text = ex.Message.ToString();
                lblMsgSis.ForeColor = Color.Red;
                lblMsgSis.Visible = true;

                prisLimpaCampos();
            }
        }

        protected void prisLimpaCampos()
        {
            txtNome.Value = string.Empty;
            txtLogin.Value = string.Empty;
            txtCargo.Value = string.Empty;
            txtCpf.Value = string.Empty;
            txtSenha.Value = string.Empty;
            txtIdade.Value = string.Empty;
        }
    }
}