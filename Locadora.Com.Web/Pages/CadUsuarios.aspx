﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Pages/Locadora.Master" AutoEventWireup="true" CodeBehind="CadUsuarios.aspx.cs" Inherits="Locadora.Com.Web.Pages.CadUsuarios" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="row">
        <div class="col-lg-12">
                <h2 class="page-header"><i class="fa fa-user"></i> Usuário <small>- cadastro</small></h2>
         </div>
        <div class="form-control"">
            <div class="form-group">
            <div class="col-md-4">
                 <label>Nome</label>                                                         
                 <input class="form-control" runat="server" placeholder="Nome Completo" name="txtNome" id="txtNome" type="text" maxlength="100" />                              
            </div>
            <div class="col-md-4">
                 <label>CPF</label>                                                         
                 <input class="form-control" runat="server" placeholder="CPF Somente números" name="txtCpf" id="txtCpf" type="text" maxlength="11" />                              
            </div>
            <div class="col-md-4">
                 <label>Idade</label>                                                         
                 <input class="form-control" runat="server" placeholder="Idade" name="txtIdade" id="txtIdade" type="text" maxlength="3" />                              
            </div>
            <div class="col-md-4">
                 <label>Cargo</label>                                                         
                 <input class="form-control" runat="server" placeholder="Cargo" name="txtCargo" id="txtCargo" type="text" maxlength="20" />                              
            </div>
            <div class="col-md-4">
                 <label>Login</label>                                                         
                 <input class="form-control" runat="server" placeholder="Login do Sistema" name="txtLogin" id="txtLogin" type="text" maxlength="50" />                              
            </div>
            <div class="col-md-4">
                 <label>Senha</label>                                                         
                 <input class="form-control" runat="server" placeholder="Cadastre uma Senha" name="txtSenha" id="txtSenha" type="password" maxlength="10" />                              
            </div>
            <br />
                <div class="form-group">
                    <div class="col-md-4">
                     <button id="btnGravar" name="btnGravar" type="submit" runat="server" class="btn btn-success pull-right" 
                         onserverclick="btnGravar_Click"><i class="fa fa-save"></i> Gravar Registro</button>
                    </div>
               </div>
                <asp:Label Text="" runat="server" id="lblMsgSis" Visible="false"/>
        </div>
      </div>
    </div>
</asp:Content>