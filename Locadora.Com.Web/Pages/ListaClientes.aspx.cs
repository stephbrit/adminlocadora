﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MOD.Locadora;
using BLL.Locadora;

namespace Locadora.Com.Web.Pages
{
    public partial class ListaClientes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            prisCarregaGrid();
        }

        protected void gridClientes_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void prisCarregaGrid(){
            try
            {
                var objBllCli = new bllClientes();

                gridClientes.DataSource = objBllCli.pubListaTodosOsClientes();
                gridClientes.DataBind();
                upPanel.Visible = true;
            }
            catch (Exception ex)
            {
                throw new Exception("Problema ao carregar eventos: - " + ex.Message.ToString());
            }
        }
    }
}