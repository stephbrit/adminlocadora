﻿using BLL.Locadora;
using MOD.Locadora;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Locadora.Com.Web
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnEntrar_Click(object sender, EventArgs e)
        {
            string login;
            string senha;
            var objBllU = new bllUsuarios();           

            List<modTbUsuarios> usuario = new List<modTbUsuarios>();

            try
            {
                login = txtLogin.Value;
                senha = txtSenha.Value;

                usuario = objBllU.pubUsuarioLogar(login, senha);

                foreach (var i in usuario)
                {
                    Session["idUsuario"] = i.idUsuario;
                    Session["login"] = i.dsLogin;
                }

                if (Session["idUsuario"] != null)
                {
                    Response.Redirect("~/Pages/Index.aspx");
                }
                else
                {
                    lblMsgSis.Text = "Usuário ou senha inválidos!";
                    lblMsgSis.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lblMsgSis.Text = ex.Message.ToString();
                lblMsgSis.Visible = true;
            }
        }
    }
}