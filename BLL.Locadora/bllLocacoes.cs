﻿using DAL.Locadora.Persistence;
using MOD.Locadora;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Locadora
{
    public class bllLocacoes : ConexaoBD
    {
        public void pubCadastraLocacao(modTbLocacao loc)
        {
            using (sqlCon = new SqlConnection(strCxRai))
            {
                if (sqlCon != null)
                {

                    objCmd = new SqlCommand("UspAppLocacaoCadastro_Sbrito", sqlCon);
                    objCmd.CommandType = CommandType.StoredProcedure;

                    objCmd.Parameters.AddWithValue("@IdFilme", loc.IdFilme);
                    objCmd.Parameters.AddWithValue("@IdCliente", loc.IdCliente);
                    objCmd.Parameters.AddWithValue("@IdUsuarioLocacao", loc.IdUsuarioLocacao);
                    objCmd.Parameters.AddWithValue("@DtLocacao", loc.DtLocacao);
                    objCmd.Parameters.AddWithValue("@DtProgramadaDevolucao", loc.DtProgramadaDevolucao);

                    try
                    {
                        sqlCon.Open();
                        objCmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {

                        throw new Exception("Erro ao tentar efetuar cadastro de locação: - " + e.Message);
                    }
                    finally
                    {
                        fechaConexao();
                    }
                }
                else
                {
                    throw new Exception("Problema com a conexão ao banco de dados!");
                }
            }
        }

        public List<modTbLocacao> pubListaTodosOsloc()
        {
            List<modTbLocacao> locacoes = new List<modTbLocacao>();

            objDr = null;

            using (sqlCon = new SqlConnection(strCxRai))
            {
                objCmd = new SqlCommand("UspAppLocacoesListaTodos_Sbrito", sqlCon);
                objCmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    sqlCon.Open();
                    objDr = objCmd.ExecuteReader(CommandBehavior.CloseConnection);

                    modTbLocacao loc = null;

                    while (objDr.Read())
                    {
                        loc = new modTbLocacao();

                        loc.IdLocacao = Convert.ToInt32(objDr["IdLocacao"]);
                        loc.DsNomeFilme = objDr["DsNomeFilme"].ToString();                        
                        loc.NmAnoLancamento = Convert.ToInt32(objDr["NmAnoLancamento"].ToString());
                        loc.DsNomeCli = objDr["DsNomeCli"].ToString();
                        loc.NmCpf = Convert.ToInt32(objDr["NmCpf"].ToString());
                        loc.nomeUsuario = objDr["UsrNome"].ToString();
                        loc.DtLocacao = Convert.ToDateTime(objDr["DtLocacao"]);
                        loc.DtLocacao = Convert.ToDateTime(objDr["DtProgramadaDevolucao"]);

                        locacoes.Add(loc);
                    }

                    return locacoes;
                }
                catch (Exception e)
                {
                    throw new Exception("Problema ao tentar listar locações cadastradas! - : " + e.Message);
                }

                finally
                {
                    fechaConexao();
                }
            }
        }
    }
}
