﻿using DAL.Locadora.Persistence;
using MOD.Locadora;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Locadora
{
    public class bllClientes : ConexaoBD
    {
        public void pubCadastraCliente(modTbClientes clientes)
        {
            using (sqlCon = new SqlConnection(strCxRai))
            {
                if (sqlCon != null)
                {                  

                    objCmd = new SqlCommand("UspAppClientesCadastro_Sbrito", sqlCon);
                    objCmd.CommandType = CommandType.StoredProcedure;

                    objCmd.Parameters.AddWithValue("@DsNomeCli", clientes.DsNomeCli);
                    objCmd.Parameters.AddWithValue("@NmCpf", clientes.NmCpf);
                    objCmd.Parameters.AddWithValue("@DsEndereco", clientes.DsEndereco);
                    objCmd.Parameters.AddWithValue("@NmCep", clientes.NmCep);

                    try
                    {
                        sqlCon.Open();
                        objCmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {

                        throw new Exception("Erro ao tentar efetuar cadastro do cliente: - " + e.Message);
                    }
                    finally
                    {
                        fechaConexao();
                    }
                }
                else
                {
                    throw new Exception("Problema com a conexão ao banco de dados!");
                }
            }
        }

        public void pubAtualizaCliente(modTbClientes clientes)
        {
            using (sqlCon = new SqlConnection(strCxRai))
            {
                if (sqlCon != null)
                {      
                    objCmd = new SqlCommand("UspAppClientesAlterar_Sbrito", sqlCon);
                    objCmd.CommandType = CommandType.StoredProcedure;

                    objCmd.Parameters.AddWithValue("@IdCliente", clientes.IdCliente);
                    objCmd.Parameters.AddWithValue("@DsNomeCli", clientes.DsNomeCli);
                    objCmd.Parameters.AddWithValue("@NmCpf", clientes.NmCpf);
                    objCmd.Parameters.AddWithValue("@DsEndereco", clientes.DsEndereco);
                    objCmd.Parameters.AddWithValue("@NmCep", clientes.NmCep);

                    try
                    {
                        sqlCon.Open();
                        objCmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {

                        throw new Exception("Problema ao tentar atualizar cliente - : " + e.Message);
                    }
                    finally
                    {
                        fechaConexao();
                    }
                }
                else
                {
                    throw new Exception("Problema com a conexão ao banco de dados!");
                }
            }
        }

        public List<modTbClientes> pubListaTodosOsClientes()
        {
            List<modTbClientes> clientes = new List<modTbClientes>();

            objDr = null;

            using (sqlCon = new SqlConnection(strCxRai))
            {
                objCmd = new SqlCommand("UspAppClientesListaTodos_Sbrito", sqlCon);
                objCmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    sqlCon.Open();
                    objDr = objCmd.ExecuteReader(CommandBehavior.CloseConnection);

                    modTbClientes cli = null;

                    while (objDr.Read())
                    {
                        cli = new modTbClientes();

                        cli.IdCliente = Convert.ToInt32(objDr["IdCliente"].ToString());
                        cli.DsNomeCli = objDr["DsNomeCli"].ToString();
                        cli.DsEndereco = objDr["DsEndereco"].ToString();
                        cli.NmCpf = Convert.ToDouble(objDr["NmCpf"].ToString());
                        cli.NmCep = objDr["NmCep"].ToString();                     

                        clientes.Add(cli);
                    }

                    return clientes;
                }
                catch (Exception e)
                {
                    throw new Exception("Problema ao tentar listar clientes cadastrados! - : " + e.Message);
                }

                finally
                {
                    fechaConexao();
                }
            }
        }

        public void pubRemoveCliente(modTbClientes clientes)
        {
            using (sqlCon = new SqlConnection(strCxRai))
            {
                if (sqlCon != null)
                {
                    objCmd = new SqlCommand("UspAppClientesRemove_Sbrito", sqlCon);
                    objCmd.CommandType = CommandType.StoredProcedure;

                    objCmd.Parameters.AddWithValue("@IdCliente", clientes.IdCliente);

                    try
                    {
                        sqlCon.Open();
                        objCmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {

                        throw new Exception("Problema ao tentar remover cliente - : " + e.Message);
                    }
                    finally
                    {
                        fechaConexao();
                    }
                }
                else
                {
                    throw new Exception("Problema com a conexão ao banco de dados!");
                }
            }
        }
    }
}
