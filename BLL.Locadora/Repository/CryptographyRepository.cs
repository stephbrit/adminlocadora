﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Locadora.Repository
{
    public class CryptographyRepository
    {
        public string codificaSenha(string senha)
        {
            try
            {               
                var criptMD5 = MD5.Create();
                
                byte[] inputTexto = Encoding.Default.GetBytes(senha);
             
                byte[] hashFinal = criptMD5.ComputeHash(inputTexto);
             
                var sb = new StringBuilder(); 
                for (int i = 0; i < hashFinal.Length; i++)
                {
                    sb.Append(hashFinal[i].ToString("x2"));
                }
                ///retorna string com formato já convertido para MD5
                return sb.ToString();
            }
            catch (Exception e)
            {
                throw new Exception("Problema ao tentar criptografar senha! " + e.Message);
            }
        }
    }
}
