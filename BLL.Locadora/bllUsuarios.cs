﻿using BLL.Locadora.Repository;
using DAL.Locadora.Persistence;
using MOD.Locadora;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Locadora
{
    public class bllUsuarios : ConexaoBD
    {
        public void pubCadastraNovoUsuario(modTbUsuarios usuarios)
        {
            using (sqlCon = new SqlConnection(strCxRai))
            {
                if (sqlCon != null)
                {
                    CryptographyRepository cript = new CryptographyRepository();

                    objCmd = new SqlCommand("UspAppUsuariosCadastro_Sbrito", sqlCon);
                    objCmd.CommandType = CommandType.StoredProcedure;

                    objCmd.Parameters.AddWithValue("@UsrNome", usuarios.nomeUsuario);
                    objCmd.Parameters.AddWithValue("@NmIdade", usuarios.idade);
                    objCmd.Parameters.AddWithValue("@NmCargo", usuarios.cargo);
                    objCmd.Parameters.AddWithValue("@NmCpf", usuarios.cpfUsuario);
                    objCmd.Parameters.AddWithValue("@DsLogin", usuarios.dsLogin);
                    objCmd.Parameters.AddWithValue("@DsSenha", cript.codificaSenha(usuarios.dsSenha));            

                    try
                    {
                        sqlCon.Open();
                        objCmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {

                        throw new Exception("Erro ao tentar efetuar cadastro do usuário: - " + e.Message);
                    }
                    finally
                    {
                        fechaConexao();
                    }
                }
                else
                {
                    throw new Exception("Problema com a conexão ao banco de dados!");
                }
            }
        }

        public void pubAtualizaUsuario(modTbUsuarios usuarios)
        {
            using (sqlCon = new SqlConnection(strCxRai))
            {
                if (sqlCon != null)
                {
                    CryptographyRepository cript = new CryptographyRepository();

                    objCmd = new SqlCommand("UspAppUsuariosAlterar_Sbrito", sqlCon);
                    objCmd.CommandType = CommandType.StoredProcedure;

                    objCmd.Parameters.AddWithValue("@IdUsuario", usuarios.idUsuario);
                    objCmd.Parameters.AddWithValue("@UsrNome", usuarios.nomeUsuario);
                    objCmd.Parameters.AddWithValue("@NmIdade", usuarios.idade);
                    objCmd.Parameters.AddWithValue("@NmCargo", usuarios.cargo);
                    objCmd.Parameters.AddWithValue("@NmCpf", usuarios.cpfUsuario);
                    objCmd.Parameters.AddWithValue("@DsLogin", usuarios.dsLogin);
                    objCmd.Parameters.AddWithValue("@DsSenha", cript.codificaSenha(usuarios.dsSenha));

                    try
                    {
                        sqlCon.Open();
                        objCmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {

                        throw new Exception("Problema ao tentar atualizar usuário - : " + e.Message);
                    }
                    finally
                    {
                        fechaConexao();
                    }
                }
                else
                {
                    throw new Exception("Problema com a conexão ao banco de dados!");
                }
            }
        }

        public List<modTbUsuarios> pubListaTodosOsUsuarios()
        {
            List<modTbUsuarios> usuarios = new List<modTbUsuarios>();

            objDr = null;

            using (sqlCon = new SqlConnection(strCxRai))
            {
                objCmd = new SqlCommand("UspAppUsuariosListaTodos_Sbrito", sqlCon);
                objCmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    sqlCon.Open();
                    objDr = objCmd.ExecuteReader(CommandBehavior.CloseConnection);

                    modTbUsuarios usr = null;

                    while (objDr.Read())
                    {
                        usr = new modTbUsuarios();

                        usr.idUsuario = Convert.ToInt32(objDr["IdUsuario"].ToString());
                        usr.nomeUsuario = objDr["UsrNome"].ToString();
                        usr.dsLogin = objDr["DsLogin"].ToString();
                        usr.cpfUsuario = Convert.ToDouble(objDr["NmCpf"].ToString());
                        usr.idade = Convert.ToInt16(objDr["NmIdade"]);
                        usr.cargo = objDr["NmCargo"].ToString();                    

                        usuarios.Add(usr);
                    }

                    return usuarios;
                }
                catch (Exception e)
                {
                    throw new Exception("Problema ao tentar listar usuários cadastrados! - : " + e.Message);
                }

                finally
                {
                    fechaConexao();
                }
            }
        }

        public List<modTbUsuarios> pubUsuarioLogar(string login, string senha)
        {
            List<modTbUsuarios> usuarios = new List<modTbUsuarios>();

            objDr = null;

            using (sqlCon = new SqlConnection(strCxRai))
            {
                CryptographyRepository cript = new CryptographyRepository();

                objCmd = new SqlCommand("UspAppUsuariosLogar_Sbrito", sqlCon);
                objCmd.CommandType = CommandType.StoredProcedure;

                objCmd.Parameters.AddWithValue("@DsLogin", login);
                objCmd.Parameters.AddWithValue("@DsSenha", cript.codificaSenha(senha));

                try
                {
                    sqlCon.Open();
                    objDr = objCmd.ExecuteReader(CommandBehavior.CloseConnection);

                    modTbUsuarios usr = null;

                    while (objDr.Read())
                    {
                        usr = new modTbUsuarios();

                        usr.idUsuario = Convert.ToInt32(objDr["IdUsuario"].ToString());                       
                        usr.dsLogin = objDr["DsLogin"].ToString();                     

                        usuarios.Add(usr);
                    }

                    return usuarios;
                }
                catch (Exception e)
                {
                    throw new Exception("Problema ao tentar efetuar login! - : " + e.Message);
                }

                finally
                {
                    fechaConexao();
                }
            }
        }

        public void pubRemoveUsuario(modTbUsuarios usuarios)
        {
            using (sqlCon = new SqlConnection(strCxRai))
            {
                if (sqlCon != null)
                {                 
                    objCmd = new SqlCommand("UspAppUsuariosRemove_Sbrito", sqlCon);
                    objCmd.CommandType = CommandType.StoredProcedure;

                    objCmd.Parameters.AddWithValue("@IdUsuario", usuarios.idUsuario);              

                    try
                    {
                        sqlCon.Open();
                        objCmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {

                        throw new Exception("Problema ao tentar atualizar usuário - : " + e.Message);
                    }
                    finally
                    {
                        fechaConexao();
                    }
                }
                else
                {
                    throw new Exception("Problema com a conexão ao banco de dados!");
                }
            }
        }
    }
}
