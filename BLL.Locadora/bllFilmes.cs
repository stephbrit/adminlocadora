﻿using DAL.Locadora.Persistence;
using MOD.Locadora;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Locadora
{
    public class bllFilmes : ConexaoBD
    {
        public void pubCadastraFilme(modTbFilmes filmes)
        {
            using (sqlCon = new SqlConnection(strCxRai))
            {
                if (sqlCon != null)
                {

                    objCmd = new SqlCommand("UspAppFilmesCadastro_Sbrito", sqlCon);
                    objCmd.CommandType = CommandType.StoredProcedure;

                    objCmd.Parameters.AddWithValue("@DsNomeCli", filmes.DsNomeFilme);
                    objCmd.Parameters.AddWithValue("@NmCpf", filmes.NmAnoLancamento);                  

                    try
                    {
                        sqlCon.Open();
                        objCmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {

                        throw new Exception("Erro ao tentar efetuar cadastro do filme: - " + e.Message);
                    }
                    finally
                    {
                        fechaConexao();
                    }
                }
                else
                {
                    throw new Exception("Problema com a conexão ao banco de dados!");
                }
            }
        }

        public List<modTbFilmes> pubListaTodosOsFilmes()
        {
            List<modTbFilmes> clientes = new List<modTbFilmes>();

            objDr = null;

            using (sqlCon = new SqlConnection(strCxRai))
            {
                objCmd = new SqlCommand("UspAppFilmesListaTodos_Sbrito", sqlCon);
                objCmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    sqlCon.Open();
                    objDr = objCmd.ExecuteReader(CommandBehavior.CloseConnection);

                    modTbFilmes film = null;

                    while (objDr.Read())
                    {
                        film = new modTbFilmes();

                        film.IdFilme = Convert.ToInt32(objDr["IdFilme"].ToString());
                        film.DsNomeFilme = objDr["DsNomeFilme"].ToString();
                        film.NmAnoLancamento = Convert.ToInt32(objDr["NmAnoLancamento"].ToString());                      

                        clientes.Add(film);
                    }

                    return clientes;
                }
                catch (Exception e)
                {
                    throw new Exception("Problema ao tentar listar filmes cadastrados! - : " + e.Message);
                }

                finally
                {
                    fechaConexao();
                }
            }
        }
    }
}
