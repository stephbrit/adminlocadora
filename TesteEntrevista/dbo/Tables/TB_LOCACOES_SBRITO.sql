﻿CREATE TABLE [dbo].[TB_LOCACOES_SBRITO] (
    [IdLocacao]             INT      IDENTITY (1, 1) NOT NULL,
    [IdCliente]             INT      NULL,
    [IdFilme]               INT      NULL,
    [DtLocacao]             DATETIME NULL,
    [IdUsuarioLocacao]      INT      NULL,
    [DtProgramadaDevolucao] DATETIME NULL,
    PRIMARY KEY CLUSTERED ([IdLocacao] ASC),
    FOREIGN KEY ([IdCliente]) REFERENCES [dbo].[TB_CLIENTES_SBRITO] ([IdCliente]),
    FOREIGN KEY ([IdFilme]) REFERENCES [dbo].[TB_FILMES_SBRITO] ([IdFilme]),
    FOREIGN KEY ([IdUsuarioLocacao]) REFERENCES [dbo].[TB_USUARIOS_SBRITO] ([IdUsuario])
);

