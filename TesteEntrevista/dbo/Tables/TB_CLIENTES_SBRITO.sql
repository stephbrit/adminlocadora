﻿CREATE TABLE [dbo].[TB_CLIENTES_SBRITO] (
    [IdCliente]  INT           IDENTITY (1, 1) NOT NULL,
    [DsNomeCli]  VARCHAR (100) NULL,
    [NmCpf]      NUMERIC (11)  NULL,
    [DsEndereco] VARCHAR (50)  NULL,
    [NmCep]      VARCHAR (9)   NULL,
    PRIMARY KEY CLUSTERED ([IdCliente] ASC),
    UNIQUE NONCLUSTERED ([NmCpf] ASC)
);

