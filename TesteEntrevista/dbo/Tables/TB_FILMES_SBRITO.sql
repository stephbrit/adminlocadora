﻿CREATE TABLE [dbo].[TB_FILMES_SBRITO] (
    [IdFilme]         INT          IDENTITY (1, 1) NOT NULL,
    [DsNomeFilme]     VARCHAR (50) NULL,
    [NmAnoLancamento] INT          NULL,
    PRIMARY KEY CLUSTERED ([IdFilme] ASC)
);

