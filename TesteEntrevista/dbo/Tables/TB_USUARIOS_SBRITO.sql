﻿CREATE TABLE [dbo].[TB_USUARIOS_SBRITO] (
    [IdUsuario] INT           IDENTITY (1, 1) NOT NULL,
    [UsrNome]   VARCHAR (100) NULL,
    [NmCpf]     NUMERIC (11)  NULL,
    [NmIdade]   SMALLINT      NULL,
    [NmCargo]   VARCHAR (20)  NULL,
    PRIMARY KEY CLUSTERED ([IdUsuario] ASC),
    UNIQUE NONCLUSTERED ([NmCpf] ASC)
);

