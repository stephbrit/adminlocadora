﻿CREATE TABLE [dbo].[TB_LOGIN_USUARIO_SBRITO] (
    [IdLogin]   INT          IDENTITY (1, 1) NOT NULL,
    [IdUsuario] INT          NULL,
    [DsLogin]   VARCHAR (50) NULL,
    [DsSenha]   VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([IdLogin] ASC),
    FOREIGN KEY ([IdUsuario]) REFERENCES [dbo].[TB_USUARIOS_SBRITO] ([IdUsuario])
);