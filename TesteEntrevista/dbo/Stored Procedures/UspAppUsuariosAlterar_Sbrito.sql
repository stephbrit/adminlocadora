﻿CREATE PROCEDURE UspAppUsuariosAlterar_Sbrito

(
	@IdUsuario INT,
	@UsrNome VARCHAR (100),
	@NmCargo VARCHAR (20),
	@NmIdade SMALLINT,
	@NmCpf NUMERIC(11),
	@DsLogin VARCHAR(50),
	@DsSenha VARCHAR(50)
)

AS

BEGIN
	 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	 SET NOCOUNT ON;

	 IF EXISTS (SELECT TOP 1 IdUsuario FROM TB_USUARIOS_SBRITO WITH (NOLOCK) WHERE IdUsuario = @IdUsuario)
	 BEGIN
		 UPDATE TB_USUARIOS_SBRITO
		 SET UsrNome = @UsrNome, NmCargo = @NmCargo, NmIdade = @NmIdade, NmCpf = @NmCpf 
		 WHERE IdUsuario = @IdUsuario

		 UPDATE TB_LOGIN_USUARIO_SBRITO
		 SET DsLogin = @DsLogin, DsSenha = @DsSenha
		 WHERE IdUsuario = @IdUsuario
	END
END