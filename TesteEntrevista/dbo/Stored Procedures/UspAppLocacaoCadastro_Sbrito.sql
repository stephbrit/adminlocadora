﻿
CREATE PROCEDURE [dbo].[UspAppLocacaoCadastro_Sbrito]

(
	@IdFilme INT,
	@IdCliente INT,
	@IdUsuarioLocacao INT,
	@DtLocacao DATETIME,
	@DtProgramadaDevolucao DATETIME
)

AS

BEGIN
     SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
     SET NOCOUNT ON;
                
        INSERT INTO TB_LOCACOES_SBRITO (IdFilme, IdCliente, IdUsuarioLocacao, DtLocacao, DtProgramadaDevolucao)
		VALUES (@IdFilme, @IdCliente, @IdUsuarioLocacao, @DtLocacao, @DtProgramadaDevolucao)
END