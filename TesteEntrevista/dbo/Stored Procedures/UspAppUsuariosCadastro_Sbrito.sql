﻿
CREATE PROCEDURE [dbo].[UspAppUsuariosCadastro_Sbrito]

(
                @UsrNome VARCHAR (100),
                @NmIdade SMALLINT,
                @NmCargo VARCHAR (20),
                @NmCpf NUMERIC (11),
				@DsLogin VARCHAR(50),
				@DsSenha VARCHAR(50)
)

AS

BEGIN
                SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
                SET NOCOUNT ON;
                
                 INSERT INTO TB_USUARIOS_SBRITO(UsrNome, NmIdade, NmCargo, NmCpf)
				 VALUES (@UsrNome, @NmIdade, @NmCargo, @NmCpf)

				 DECLARE @IdUsuarioUltimoCad INT

				 SET @IdUsuarioUltimoCad = (SELECT TOP 1 IdUsuario FROM TB_USUARIOS_SBRITO WITH (NOLOCK) ORDER BY IdUsuario DESC)

				 INSERT INTO TB_LOGIN_USUARIO_SBRITO (IdUsuario, DsLogin, DsSenha)
				 VALUES (@IdUsuarioUltimoCad, @DsLogin, @DsSenha)
END
