﻿
CREATE PROCEDURE [dbo].[UspAppUsuariosListaTodos_Sbrito]

AS

BEGIN
     SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
     SET NOCOUNT ON;
                
                SELECT 
					  U.IdUsuario,
					  U.UsrNome,
					  L.DsLogin,
					  U.NmCpf,
					  U.NmIdade,
					  U.NmCargo
				FROM TB_USUARIOS_SBRITO AS U WITH (NOLOCK)
					 INNER JOIN TB_LOGIN_USUARIO_SBRITO AS L WITH (NOLOCK)
				ON L.IdUsuario = U.IdUsuario
                ORDER BY UsrNome ASC
END
