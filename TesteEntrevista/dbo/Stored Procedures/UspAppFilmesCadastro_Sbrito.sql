﻿
CREATE PROCEDURE [dbo].[UspAppFilmesCadastro_Sbrito]

(
	@DsNomeFilme VARCHAR (40),
	@NmAnoLancamento INT
)

AS

BEGIN
     SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
     SET NOCOUNT ON;
                
        INSERT INTO TB_FILMES_SBRITO (DsNomeFilme, NmAnoLancamento)
		VALUES (@DsNomeFilme, @NmAnoLancamento)
END