﻿
CREATE PROCEDURE [dbo].[UspAppLocacoesListaTodos_Sbrito]

AS

BEGIN
	 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	 SET NOCOUNT ON;

		 SELECT 
			  L.IdLocacao,
			  F.DsNomeFilme,
			  F.NmAnoLancamento,
			  C.DsNomeCli,
			  C.NmCpf,
			  U.UsrNome,
			  L.DtLocacao,
			  L.DtProgramadaDevolucao
		 FROM TB_LOCACOES_SBRITO AS L WITH (NOLOCK)
			  INNER JOIN TB_FILMES_SBRITO AS F WITH (NOLOCK)
		 ON L.IdFilme = F.IdFilme
			  INNER JOIN TB_CLIENTES_SBRITO AS C WITH (NOLOCK)
		 ON C.IdCliente = L.IdCliente
			  INNER JOIN TB_USUARIOS_SBRITO AS U WITH (NOLOCK)
		 ON U.IdUsuario = L.IdUsuarioLocacao
END