﻿
create PROCEDURE [dbo].[UspAppClientesAlterar_Sbrito]

(
				@IdCliente INT,
                @DsNomeCli VARCHAR (100),              
                @NmCpf NUMERIC (11),
				@DsEndereco VARCHAR(50),
				@NmCep VARCHAR(9)
)

AS

BEGIN
     IF EXISTS (SELECT TOP 1 IdCliente FROM TB_CLIENTES_SBRITO WITH (NOLOCK) WHERE IdCliente = @IdCliente)
	 BEGIN
		 UPDATE TB_CLIENTES_SBRITO
		 SET DsNomeCli = @DsNomeCli, DsEndereco = @DsEndereco, NmCpf = @NmCpf, NmCep = @NmCep 
		 WHERE IdCliente = @IdCliente	
	END				 
END