﻿
create PROCEDURE [dbo].[UspAppClientesCadastro_Sbrito]

(
                @DsNomeCli VARCHAR (100),              
                @NmCpf NUMERIC (11),
				@DsEndereco VARCHAR(50),
				@NmCep VARCHAR(9)
)

AS

BEGIN
                SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
                SET NOCOUNT ON;
                
                 INSERT INTO TB_CLIENTES_SBRITO(DsNomeCli, DsEndereco, NmCpf, NmCep)
				 VALUES (@DsNomeCli, @DsEndereco, @NmCpf, @NmCep)				 
END