﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Locadora.Persistence
{
    public class ConexaoBD
    {
        //protected string strCxInterno = "Data Source=(localdb);Initial Catalog = TesteEntrevista; Integrated Security = True; Connect Timeout = 30; Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        protected string strCxRai = "Data Source=gruporai.clientes.ananke.com.br;Initial Catalog=TesteEntrevista;Integrated Security=False; user id=TesteEntrevista; password=8fBvjKm3";
        protected SqlConnection sqlCon;
        protected SqlCommand objCmd;
        protected SqlDataReader objDr;  

        protected void abreConexao()
        {
            try
            {
                sqlCon = new SqlConnection(strCxRai);

                sqlCon.Open();
            }

            catch (SqlException ex)
            {

                throw new Exception("Problema de conexão com o servidor SQL - " + ex.Number + " - Descrição: " + ex.Message.ToString());
            }

            catch (Exception e)
            {
                throw new Exception("Erro : - " + e.Message);
            }
        }
        
        protected void fechaConexao()
        {
            try
            {
                sqlCon.Close();
            }
            catch (SqlException ex)
            {
                throw new Exception("[Erro ao encerrar conexão com o banco de dados] - : " + ex.Number + " - Descrição: " + ex.Message.ToString());
            }

            catch (Exception e)
            {

                throw new Exception("Problema ao encerrar conexão:  - " + e.Message);
            }
        }
    }
}
