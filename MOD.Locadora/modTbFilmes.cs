﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MOD.Locadora
{
    public class modTbFilmes
    {
       private int _IdFilme;
       private string _DsNomeFilme;
       private int _NmAnoLancamento;
    
       public int IdFilme
       {
            get { return _IdFilme; }
            set { _IdFilme = value; }
       }
        public string DsNomeFilme
        {
            get { return _DsNomeFilme; }
            set { _DsNomeFilme = value; }
        }
        public int NmAnoLancamento
        {
            get { return _NmAnoLancamento; }
            set { _NmAnoLancamento = value; }
        }
    }
}
