﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MOD.Locadora
{
    public class modTbClientes
    {
        private int _IdCliente;
        private string _DsNomeCli;
        private double _NmCpf;
        private string _DsEndereco;
        private string _NmCep;

        public int IdCliente
        {
            get { return _IdCliente; }
            set { _IdCliente = value; }
        }
        public string DsNomeCli
        {
            get { return _DsNomeCli; }
            set { _DsNomeCli = value; }
        }
        public double NmCpf
        {
            get { return _NmCpf; }
            set { _NmCpf = value; }
        }
        public string DsEndereco
        {
            get { return _DsEndereco; }
            set { _DsEndereco = value; }
        }
        public string NmCep
        {
            get { return _NmCep; }
            set { _NmCep = value; }
        }
    }
}
