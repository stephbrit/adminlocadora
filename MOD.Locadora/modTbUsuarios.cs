﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MOD.Locadora
{
    public class modTbUsuarios
    {
        private int _idUsuario;
        private string _nomeUsuario;
        private double _cpfUsuario;
        private short _idade;
        private string _cargo;
        private string _dsLogin;
        private string _senhaUsr;

        public int idUsuario
        {
            get { return _idUsuario; }
            set { _idUsuario = value; }
        }
        public string nomeUsuario
        {
            get { return _nomeUsuario; }
            set { _nomeUsuario = value; }
        }
        public double cpfUsuario
        {
            get { return _cpfUsuario; }
            set { _cpfUsuario = value; }
        }
        public short idade
        {
            get { return _idade; }
            set { _idade = value; }
        }
        public string cargo
        {
            get { return _cargo; }
            set { _cargo = value; }
        }

        public string dsLogin
        {
            get { return _dsLogin; }
            set { _dsLogin = value; }
        }

        public string dsSenha
        {
            get { return _senhaUsr; }
            set { _senhaUsr = value; }
        }
    }
}
