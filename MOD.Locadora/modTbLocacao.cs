﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MOD.Locadora
{
    public class modTbLocacao
    {
        private int _IdLocacao;
        private int _IdCliente;
        private int _IdFilme;
        private DateTime _DtLocacao;
        private int _IdUsuarioLocacao;
        private DateTime _DtProgramadaDevolucao;
        private string _DsNomeFilme;
        private int _NmAnoLancamento;
        private string _DsNomeCli;
        private double _NmCpf;
        private string _nomeUsuario;

        public modTbLocacao()
        {
            DtLocacao = DateTime.Now;
        }

        public int IdLocacao
        {
            get { return _IdLocacao; }
            set { _IdLocacao = value; }
        }
        public int IdCliente
        {
            get { return _IdCliente; }
            set { _IdCliente = value; }
        }
        public int IdFilme
        {
            get { return _IdFilme; }
            set { _IdFilme = value; }
        }
        public DateTime DtLocacao
        {
            get { return _DtLocacao; }
            set { _DtLocacao = value; }
        }
        public int IdUsuarioLocacao
        {
            get { return _IdUsuarioLocacao; }
            set { _IdUsuarioLocacao = value; }
        }
        public DateTime DtProgramadaDevolucao
        {
            get { return _DtProgramadaDevolucao; }
            set { _DtProgramadaDevolucao = value; }
        }
        public string DsNomeFilme
        {
            get { return _DsNomeFilme; }
            set { _DsNomeFilme = value; }
        }
        public int NmAnoLancamento
        {
            get { return _NmAnoLancamento; }
            set { _NmAnoLancamento = value; }
        }
        public string DsNomeCli
        {
            get { return _DsNomeCli; }
            set { _DsNomeCli = value; }
        }
        public double NmCpf
        {
            get { return _NmCpf; }
            set { _NmCpf = value; }
        }
        public string nomeUsuario
        {
            get { return _nomeUsuario; }
            set { _nomeUsuario = value; }
        }
    }
}
